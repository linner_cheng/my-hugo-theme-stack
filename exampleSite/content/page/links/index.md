---
title: "链接"
links:
  - title: Jackcin's Blog
    description: 友链：Jackcin的个人博客首页
    website: https://jackcin877.github.io/vuepress-theme-vdoing/
    image: https://jackcin877.github.io/vuepress-theme-vdoing/img/logo.png
  - title: GitHub
    #description: GitHub is the world's largest software development platform.
    website: https://github.com
    image: https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png
  # - title: TypeScript
  #   description: TypeScript is a typed superset of JavaScript that compiles to plain JavaScript.
  #   website: https://www.typescriptlang.org
  #   image: ts-logo-128.jpg
  - title: 菜鸟教程
    website: https://www.runoob.com/
    image: https://static.runoob.com/images/icon/mobile-icon.png
  - title: Gitee 码云
    website: https://gitee.com/
    image: https://gitee.com/static/images/logo_themecolor.png
  - title: 哔哩哔哩
    website: https://www.bilibili.com/
    image: https://static.hdslb.com/mobile/img/512.png
  - title: 知乎
    website: https://www.zhihu.com/
    image: https://static.zhihu.com/heifetz/assets/apple-touch-icon-152.81060cab.png
  - title: Linner's Blog
    description: 旧版博客主页，已停止更新。
    website: https://old.blog.linner.asia/
    image: https://old.blog.linner.asia/favicon.ico
menu:
    main: 
        weight: -50
        params:
            icon: link

comments: false
---
