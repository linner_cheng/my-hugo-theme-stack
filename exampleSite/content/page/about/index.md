---
title: 关于
# description: Hugo, the world's fastest framework for building websites
date: 2022-09-02 16:04:59
aliases:
  - about-us
  - about-hugo
  - contact
#license: CC BY-NC-ND
#lastmod: '2020-10-09'
menu:
    main: 
        weight: -55
        params:
            icon: user
links:
  - title: 我的邮箱
    description: linner.cheng@qq.com。关于文章内容，欢迎通过邮箱帮助我改进。
    website: mailto:linner.cheng@qq.com?subject=About Linner's Blog
  - title: GitHub
    #description: GitHub is the world's largest software development platform.
    website: https://github.com/Linna-cy
    description: 我的Github主页
    image: https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png
  - title: Gitee 码云
    description: 我的Gitee主页
    website: https://gitee.com/linner_cheng
    image: https://gitee.com/static/images/logo_themecolor.png
  # - title: 哔哩哔哩
  #   website: https://www.bilibili.com/
  #   image: https://static.hdslb.com/mobile/img/512.png
  - title: 知乎
    description: 我的知乎主页（目前还没有在知乎发表过文章）
    website: https://www.zhihu.com/people/chen-yu-da-ren
    image: https://static.zhihu.com/heifetz/assets/apple-touch-icon-152.81060cab.png
  - title: Linner's Blog
    description: 旧版博客主页，已停止更新。
    website: https://old.blog.linner.asia/
    image: https://old.blog.linner.asia/favicon.ico
  - title: Hugo Theme Stack
    description: 获取本站主题
    website: https://github.com/CaiJimmy/hugo-theme-stack
  - title: Hugo
    description: 本站使用Hugo构建
    website: https://github.com/gohugoio/hugo
  - title: My Hugo Theme Stack
    description: 本站Stack主题配置
    website: https://gitee.com/linner_cheng/my-hugo-theme-stack
  - title: GitHub Pages
    description: 本站源码
    website: https://github.com/Linna-cy/Linna-cy.github.io
---

大三在校生，目前就读广州工商学院，软件工程专业。以下是与我或本站有关的链接。
